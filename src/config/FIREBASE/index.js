import firebase from "firebase";

firebase.initializeApp({
  apiKey: "AIzaSyC1QzKX3NJNsDW9i-Zl_nzSrnto1mdfrfE",
  authDomain: "temukand-d8854.firebaseapp.com",
  projectId: "temukand-d8854",
  storageBucket: "temukand-d8854.appspot.com",
  messagingSenderId: "823625233678",
  appId: "1:823625233678:web:1739e606c1f7b0064b935f",
  measurementId: "G-0SEEF39E4E",
});

const FIREBASE = firebase;
export default FIREBASE;
