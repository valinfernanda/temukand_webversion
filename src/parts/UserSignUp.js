import React, { Component } from "react";
// import { onChange } from "react-native-reanimated";
// import { connect } from "react-redux";
// import { getProvinsiList } from "../../src/store/actions/rajaongkir";

class UserSignUp extends Component {
  //   componentDidMount() {
  //     this.props.dispatch(getProvinsiList());
  //   }

  render() {
    return (
      <section id="contact">
        <div class="container">
          <div class="row text-center mb-3">
            <div class="col">
              <h2>Mulai pencarian anda bersama Temukand.</h2>
            </div>
          </div>
          <div class="row justify-content-center">
            <div class="col-md-6 ">
              <form>
                <div class="mb-3">
                  <label for="email" class="form-label">
                    Nama Lengkap
                  </label>
                  <input
                    type="name"
                    class="form-control"
                    id="name"
                    aria-describedby="name"
                    // value={name}
                    // onChange={onChange}
                  />
                </div>
                <div class="mb-3">
                  <label for="email" class="form-label">
                    Email
                  </label>
                  <input
                    type="email"
                    class="form-control"
                    id="email"
                    aria-describedby="email"
                  />
                </div>
                <div class="mb-3">
                  <label for="email" class="form-label">
                    Password
                  </label>
                  <input
                    type="password"
                    class="form-control"
                    id="password"
                    aria-describedby="password"
                  />
                </div>
                <div class="mb-3">
                  <label for="email" class="form-label">
                    Confirm Password
                  </label>
                  <input
                    type="password"
                    class="form-control"
                    id="password"
                    aria-describedby="password"
                  />
                </div>

                <button type="submit" class="btn btn-primary">
                  Submit
                </button>
              </form>
            </div>
          </div>
        </div>
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
          <path
            fill="#125889"
            fill-opacity="1"
            d="M0,288L48,272C96,256,192,224,288,224C384,224,480,256,576,250.7C672,245,768,203,864,192C960,181,1056,203,1152,197.3C1248,192,1344,160,1392,144L1440,128L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"
          ></path>
        </svg>
      </section>
    );
  }
}

export default UserSignUp;
