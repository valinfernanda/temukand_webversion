import React from "react";
import Fade from "react-reveal/Fade";
import Button from "elements/Button";
import Nanny from "./../assets/images/nanny1.jpg";

export default function ListMaid(props) {
  return (
    <div>
      <div>
        <h2 class="text-center mt-5">Featured Profile</h2>
        {/* <h3>Total : 34</h3> */}
      </div>

      <div id="myCarousel" class="carousel slide" data-bs-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <div class="rowListMaid">
              <div class="containerneumorphism">
                <div class="neumorphism-1">
                  <div class="dataMaid mt-3 ml-3 mx-auto">
                    <img src={Nanny} alt="nanny" className="img-cover" />
                  </div>
                  <div class="desc mx-auto">
                    <div class="nama mt-2">Fitri</div>
                    <div class="location">Bandung, Jawa Barat</div>
                    <div class="skill mt-3 ml-3 mr-3">
                      <p>
                        <em>
                          "Saya bisa memasak, nyapu, ngepel cuci piring dan
                          bikin makanan sehat untuk keluarga"
                        </em>
                      </p>
                    </div>
                    <Button class="mx-auto" type="link" href="#">
                      Read More...
                    </Button>
                  </div>
                </div>
              </div>

              <div class="containerneumorphism">
                <div class="neumorphism-1">
                  <div class="dataMaid mt-3 ml-3 mx-auto">
                    <img src={Nanny} alt="nanny" className="img-cover" />
                  </div>
                  <div class="desc mx-auto">
                    <div class="nama mt-2">Fitri</div>
                    <div class="location">Bandung, Jawa Barat</div>
                    <div class="skill mt-3 ml-3 mr-3">
                      <p>
                        <em>
                          "Saya bisa memasak, nyapu, ngepel cuci piring dan
                          bikin makanan sehat untuk keluarga"
                        </em>
                      </p>
                    </div>
                    <Button class="mx-auto" type="link" href="#">
                      Read More...
                    </Button>
                  </div>
                </div>
              </div>

              <div class="containerneumorphism">
                <div class="neumorphism-1">
                  <div class="dataMaid mt-3 ml-3 mx-auto">
                    <img src={Nanny} alt="nanny" className="img-cover" />
                  </div>
                  <div class="desc mx-auto">
                    <div class="nama mt-2">Fitri</div>
                    <div class="location">Bandung, Jawa Barat</div>
                    <div class="skill mt-3 ml-3 mr-3">
                      <p>
                        <em>
                          "Saya bisa memasak, nyapu, ngepel cuci piring dan
                          bikin makanan sehat untuk keluarga"
                        </em>
                      </p>
                    </div>
                    <Button class="mx-auto" type="link" href="#">
                      Read More...
                    </Button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="carousel-item">
            <div class="container">
              <h1>Ini slide 2</h1>
              <p>yuhuuuuuuuwyuwy</p>
            </div>
          </div>
          <div class="carousel-item">
            <div class="container">
              <h1>Ini slide 3</h1>
              <p>yuhuuuuuuuwyuwy</p>
            </div>
          </div>
        </div>
        <a
          href="#myCarousel"
          class="carousel-control-prev"
          role="button"
          data-slide="prev"
        >
          <span class="sr-only">Previous</span>
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        </a>
        <a
          href="#myCarousel"
          class="carousel-control-next"
          role="button"
          data-slide="next"
        >
          <span class="sr-only">Previous</span>
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
        </a>
      </div>

      <div>
        {/* <div class="rowListMaid">
          <div class="containerneumorphism">
            <div class="neumorphism-1">
              <div class="dataMaid mt-3 ml-3 mx-auto">
                <img src={Nanny} alt="nanny" className="img-cover" />
              </div>
              <div class="desc mx-auto">
                <div class="nama mt-2">Fitri</div>
                <div class="location">Bandung, Jawa Barat</div>
                <div class="skill mt-3 ml-3 mr-3">
                  <p>
                    <em>
                      "Saya bisa memasak, nyapu, ngepel cuci piring dan bikin
                      makanan sehat untuk keluarga"
                    </em>
                  </p>
                </div>
                <Button class="mx-auto" type="link" href="#">
                  Read More...
                </Button>
              </div>
            </div>
          </div>

          <div class="containerneumorphism">
            <div class="neumorphism-1">
              <div class="dataMaid mt-3 ml-3 mx-auto">
                <img src={Nanny} alt="nanny" className="img-cover" />
              </div>
              <div class="desc mx-auto">
                <div class="nama mt-2">Fitri</div>
                <div class="location">Bandung, Jawa Barat</div>
                <div class="skill mt-3 ml-3 mr-3">
                  <p>
                    <em>
                      "Saya bisa memasak, nyapu, ngepel cuci piring dan bikin
                      makanan sehat untuk keluarga"
                    </em>
                  </p>
                </div>
                <Button class="mx-auto" type="link" href="#">
                  Read More...
                </Button>
              </div>
            </div>
          </div>

          <div class="containerneumorphism">
            <div class="neumorphism-1">
              <div class="dataMaid mt-3 ml-3 mx-auto">
                <img src={Nanny} alt="nanny" className="img-cover" />
              </div>
              <div class="desc mx-auto">
                <div class="nama mt-2">Fitri</div>
                <div class="location">Bandung, Jawa Barat</div>
                <div class="skill mt-3 ml-3 mr-3">
                  <p>
                    <em>
                      "Saya bisa memasak, nyapu, ngepel cuci piring dan bikin
                      makanan sehat untuk keluarga"
                    </em>
                  </p>
                </div>
                <Button class="mx-auto" type="link" href="#">
                  Read More...
                </Button>
              </div>
            </div>
          </div>
        </div> */}

        <div class="text-center mt-2">
          <Button
            className="btn px-5 mb-5"
            hasShadow
            isPrimary
            // onClick={showMostPicked}
          >
            View All Profiles
          </Button>
        </div>
      </div>
    </div>
  );
}
