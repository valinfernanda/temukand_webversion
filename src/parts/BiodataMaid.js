import React from "react";
import Nanny from "./../assets/images/nanny1.jpg";
import VideoMaid from "./VideoMaid";

function BiodataMaid() {
  return (
    <div className="biodata">
      <section className="container pt-4">
        <div className="row">
          <div className="col-auto ml-5" style={{ width: 360 }}>
            <VideoMaid />
          </div>
          <div className="col" style={{ width: 530 }}>
            <div className="namadanfoto">
              <div>
                <h1>Fatimah</h1>
                <div>Bandung, 37 tahun</div>
              </div>

              <img
                src={Nanny}
                alt="nanny"
                className="foto rounded-circle img-thumbnail"
              />
            </div>
            <h4>
              <i>
                "I am hardworker and fast learner. Punya 3 anak dan
                blablabla..."{" "}
              </i>
            </h4>
            <div>
              <button
                type="button"
                class="btn rounded btn-primary btn-sm ml-2 mt-2"
              >
                Bisa bahasa asing
              </button>
              <button
                type="button"
                class="btn rounded btn-primary btn-sm ml-2 mt-2"
              >
                Lulusan ekonomi
              </button>
              <button
                type="button"
                class="btn rounded btn-primary btn-sm ml-2 mt-2"
              >
                Senang berkebun
              </button>
              <button
                type="button"
                class="btn rounded btn-primary btn-sm ml-2 mt-2"
              >
                Bisa bahasa Inggris
              </button>
              <button
                type="button"
                class="btn rounded btn-primary btn-sm ml-2 mt-2"
              >
                Lulusan IT
              </button>
              <button
                type="button"
                class="btn rounded btn-primary btn-sm ml-2 mt-2"
              >
                Bisa merawat anjing
              </button>
              <button
                type="button"
                class="btn rounded btn-primary btn-sm ml-2 mt-2"
              >
                Bisa memasak olahan babi
              </button>
              <button
                type="button"
                class="btn rounded btn-primary btn-sm ml-2 mt-2"
              >
                Tidak makan olahan babi
              </button>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}

export default BiodataMaid;
