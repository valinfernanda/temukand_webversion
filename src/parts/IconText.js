import React from "react";
import LogoTemukand from "assets/images/LogoTemukand.jpg";

import Button from "elements/Button";

export default function IconText() {
  return (
    <Button className="brand-text-icon" href="" type="link">
      <img
        className="mr-2"
        width="60"
        height="60"
        src={LogoTemukand}
        alt="logoTemukand"
      />
      Temu<span className="text-gray-900">kand.</span>
    </Button>
  );
}
