import React from "react";
import bgHero from "../../src/assets/images/maidhero.png";

export default function HeroMaid() {
  return (
    <div class="hero-maid">
      <body>
        <div class="container">
          <div class="row align-items-start custom-section">
            <div class="col">
              <h2 class="mt-5">Because of Maid.. </h2>
              <h3>Your life will really be less stressful</h3>
              <p>
                Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                Sapiente odio ex voluptatibus eum deleniti, excepturi, ullam rem
                similique dolores assumenda, non quia aliquid tempora quo.
              </p>

              <a href="#">Read More</a>
            </div>
            <div class="col">
              <img width="600" height="600" src={bgHero} alt="heromaid" />
            </div>
          </div>
        </div>
      </body>
    </div>
  );
}
