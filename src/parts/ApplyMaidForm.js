import React, { Component } from "react";
import { connect } from "react-redux";
import { getProvinsiList } from "../../src/store/actions/rajaongkir";

class ApplyMaidForm extends Component {
  componentDidMount() {
    this.props.dispatch(getProvinsiList())
  }

  render() {
    return (
      <section id="contact">
        <div class="container">
          <div class="row text-center mb-3">
            <div class="col">
              <h2>Daftar maid sebagai mitra kami sekarang</h2>
            </div>
          </div>
          <div class="row justify-content-center">
            <div class="col-md-6 ">
              <form>
                <div class="row">
                  <div class="mb-3 col-4">
                    <label for="name" class="form-label">
                      Nama Depan
                    </label>
                    <input
                      type="text"
                      class="form-control"
                      id="name"
                      aria-describedby="emailHelp"
                    />
                  </div>
                  <div class="mb-3 col-4">
                    <label for="name" class="form-label">
                      Nama Belakang
                    </label>
                    <input
                      type="text"
                      class="form-control"
                      id="name"
                      aria-describedby="emailHelp"
                    />
                  </div>
                </div>
                <div class="mb-3">
                  <label for="email" class="form-label">
                    Email
                  </label>
                  <input
                    type="email"
                    class="form-control"
                    id="email"
                    aria-describedby="email"
                  />
                </div>
                <div class="row">
                  <div class="col">
                    <div class="mb-3">
                      <label for="email" class="form-label">
                        Province
                      </label>
                      <br></br>
                      <select class="form-select" id="autoSizingSelect">
                        <option selected>Choose...</option>
                        <option value="1">Kurang dari 3 tahun</option>
                        <option value="2">Lebih dari 3 tahun</option>
                        <option value="3">Three</option>
                      </select>
                    </div>
                  </div>
                  <div class="col">
                    <div class="mb-3">
                      <label for="email" class="form-label">
                        Status
                      </label>
                      <br></br>
                      <select class="form-select" id="autoSizingSelect">
                        <option selected>Pilih</option>
                        <option value="1">Lajang</option>
                        <option value="2">Menikah</option>
                        <option value="3">Janda/ Duda</option>
                      </select>
                    </div>
                  </div>

                  <div class="col">
                    <div class="mb-3">
                      <label for="email" class="form-label">
                        Agama
                      </label>
                      <br></br>
                      <select class="form-select" id="autoSizingSelect">
                        <option selected>Pilih</option>
                        <option value="1">Islam</option>
                        <option value="2">Kristen</option>
                        <option value="3">Katholik</option>
                        <option value="4">dll</option>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="mb-3">
                  <label for="email" class="form-label">
                    Status
                  </label>
                  <input
                    type="email"
                    class="form-control"
                    id="email"
                    aria-describedby="email"
                  />
                </div>

                <div class="mb-3">
                  <label for="message" class="form-label">
                    Message
                  </label>
                  <textarea
                    class="form-control"
                    id="message"
                    rows="3"
                  ></textarea>
                </div>

                <button type="submit" class="btn btn-primary">
                  Submit
                </button>
              </form>
            </div>
          </div>
        </div>
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
          <path
            fill="#125889"
            fill-opacity="1"
            d="M0,288L48,272C96,256,192,224,288,224C384,224,480,256,576,250.7C672,245,768,203,864,192C960,181,1056,203,1152,197.3C1248,192,1344,160,1392,144L1440,128L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"
          ></path>
        </svg>
      </section>
    );
  }
}

export default connect()(ApplyMaidForm);
