import React from "react";

export default function MaidFilter() {
  return (
    <div style={{ height: 500 }}>
      <div class="mt-20">
        <h4 class="text-center">Cari berdasarkan kriteria Anda</h4>
        <section className="container" style={{ width: "400px" }}>
          <select
            class="form-select form-select-lg mb-2"
            style={{ width: "300px" }}
            aria-label=".form-select-lg example"
          >
            <option selected>Open this select menu</option>
            <option value="1">One</option>
            <option value="2">Two</option>
            <option value="3">Three</option>
          </select>

          <select
            class="form-select form-select-sm mb-2"
            style={{ width: "300px" }}
            aria-label=".form-select-sm example"
          >
            <option selected>Kebangsaan</option>
            <option value="1">Indonesia</option>
            <option value="2">Thailand</option>
            <option value="3">Malaysia</option>
          </select>

          <select
            class="form-select form-select-sm mb-2"
            style={{ width: "300px" }}
            aria-label=".form-select-sm example"
          >
            <option selected>Lokasi</option>
            <option value="1">Jakarta</option>
            <option value="2">Jakarta Selatan</option>
            <option value="3">Jakarta Pusat</option>
            <option value="3">Jakarta Barat</option>
            <option value="3">Jakarta Utara</option>
            <option value="3">Jakarta Timur</option>
            <option value="3">Tangerang</option>
            <option value="3">Bogor</option>
            <option value="3">Bekasi</option>
            <option value="3">Bandung</option>
          </select>

          <select
            class="form-select form-select-sm mb-2"
            style={{ width: "300px" }}
            aria-label=".form-select-sm example"
          >
            <option selected>Lokasi</option>
            <option value="1">Jakarta</option>
            <option value="2">Jakarta Selatan</option>
            <option value="3">Jakarta Pusat</option>
            <option value="3">Jakarta Barat</option>
            <option value="3">Jakarta Utara</option>
            <option value="3">Jakarta Timur</option>
            <option value="3">Tangerang</option>
            <option value="3">Bogor</option>
            <option value="3">Bekasi</option>
            <option value="3">Bandung</option>
          </select>

          <select
            class="form-select form-select-sm mb-2"
            style={{ width: "300px" }}
            aria-label=".form-select-sm example"
          >
            <option selected>Waktu Bekerja</option>
            <option value="1">Penuh Waktu</option>
            <option value="2">Paruh Waktu</option>
          </select>

          <div class="btn" aria-label="Basic example">
            <button type="button" class="btn btn-primary">
              Cari Sekarang
            </button>
          </div>
        </section>
      </div>
    </div>
  );
}
