import React, { useState } from "react";
import FaqsMaid2 from "./FaqsMaid2";

function FaqsMaid() {
  const [faqs, setfaqs] = useState([
    {
      question: "Berapa besar pembayaran deposit dan fee aplikasi?",
      answer:
        "Deposit, besar pembayaran deposit sesuai yang disepakati bersama dengan admin kami",
      open: true,
    },
    {
      question: "Apa yang terjadi jika maid mundur dari pekerjaannya??",
      answer:
        "Maid dibayar proporsional sesuai dengan jumlah hari kerja yang harus dibayarkan.",
      open: false,
    },
    {
      question:
        "Apabila Maid tidak hadir sesuai perjanjian, apakah uang deposit dapat dikembalikan penuh?",
      answer:
        "Uang kembali 100% dikurangi biaya pajak, biaya admin transfer apabila berbeda bank dan dipotong biaya margin & layanan aplikasi.",
      open: false,
    },
    {
      question:
        "Bagaimana jika terjadi tindak kriminal(pencurian, perusakan, atau hal-hal yang merugikan host?",
      answer:
        "Hal-hal yang diluar kendali pihak ketiga(PT HMB) bukan merupakan tanggung jawab pihak ketiga.",
      open: false,
    },
  ]);

  const toggleFAQ = (index) => {
    setfaqs(
      faqs.map((faq, i) => {
        if (i === index) {
          faq.open = !faq.open;
        } else {
          faq.open = false;
        }

        return faq;
      })
    );
  };
  return (
    <div>
      <div className="headerFaq">FAQs</div>
      <div className="faqs">
        {faqs.map((faq, i) => (
          <FaqsMaid2 faq={faq} index={i} toggleFAQ={toggleFAQ} />
        ))}
      </div>
    </div>
  );
}

export default FaqsMaid;
