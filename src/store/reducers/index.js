import { combineReducers } from "redux";
import checkout from "./checkout";
import page from "./page";
import rajaongkir from "./rajaongkir";

export default combineReducers({
  checkout,
  page,
  rajaongkir,
});
