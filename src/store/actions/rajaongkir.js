import axios from "axios";
import {
  API_HEADER_RAJAONGKIR,
  API_RAJAONGKIR,
  API_TIMEOUT,
} from "../../utils/rajaOngkir/index";

export const GET_PROVINSI = "GET_PROVINSI";

export const getProvinsiList = () => {
  console.log("Masuk action");
  return (dispatch) => {
    //LOADING
    dispatch({
      type: GET_PROVINSI,
      payload: {
        loading: true,
        data: false,
        errorMessage: false,
      },
    });

    axios({
      method: "get",
      url: API_RAJAONGKIR + "province",
      timeout: API_TIMEOUT,
      headers: API_HEADER_RAJAONGKIR,
    })
      // axios
      //   .get(API_RAJAONGKIR)
      .then((response) => {
        console.log("Response axios: ", response);

        if (response.status !== 200) {
          //ERROR
          dispatch({
            type: GET_PROVINSI,
            payload: {
              loading: false,
              data: false,
              errorMessage: response,
            },
          });
        } else {
          console.log("Response berhasil: ", response.data.rajaongkir.results);

          //BERHASIL
          dispatch({
            type: GET_PROVINSI,
            payload: {
              loading: false,
              data: response.data ? response.data.rajaongkir.results : [],
              errorMessage: false,
            },
          });
        }
      })
      .catch((error) => {
        //ERROR
        dispatch({
          type: GET_PROVINSI,
          payload: {
            loading: false,
            data: false,
            errorMessage: error,
          },
        });

        alert(error, "ini error");
      });
  };
};
