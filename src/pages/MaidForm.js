import React, { Component } from "react";
import Header from "parts/Header";
import Footer from "parts/Footer";
import ApplyMaidForm from "parts/ApplyMaidForm";

export default class MaidForm extends Component {
  componentDidMount() {
    window.title = "Temukand | Maid";
    window.scrollTo(0, 0);
  }
  render() {
    return (
      <>
        <Header {...this.props}></Header>
        <ApplyMaidForm />

        <Footer />
      </>
    );
  }
}
