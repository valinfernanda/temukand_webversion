import Header from "parts/Header";
import React, { Component } from "react";
import Footer from "parts/Footer";
import BiodataMaid from "parts/BiodataMaid";
import Fade from "react-reveal/Fade";
import BookingForm from "parts/BookingForm";

import PageDetailDescription from "parts/PageDetailDescription";

export default class MaidDetails extends Component {
  componentDidMount() {
    window.title = "Temukand | Maid";
    window.scrollTo(0, 0);
  }
  render() {
    const breadcrumb = [
      { pageTitle: "Maid", pageHref: "" },
      { pageTitle: "Maid List", pageHref: "" },
    ];

    const settings = {
      dots: true,
      autoplay: true,
      infinite: true,
      slidesToShow: 2,
      slidesToScroll: 1,
    };
    return (
      <>
        <Header {...this.props}></Header>
        <Fade bottom>
          <BiodataMaid />
        </Fade>
        <section className="container box">
          <div className="row kotakdetail">
            <div className="col-7 pr-5 box1">
              <Fade bottom>
                <div className="row kotakdetail2">
                  <div className="col mt-3 font-weight-bold">Pengalaman </div>
                  <div className="col mt-3">: 6 tahun</div>
                </div>
                <div className="row kotakdetail2">
                  <div className="col mt-3 font-weight-bold">
                    Salary expected{" "}
                  </div>
                  <div className="col mt-3">: 3 juta</div>
                </div>
                <div className="row kotakdetail2">
                  <div className="col mt-3 font-weight-bold">Agama </div>
                  <div className="col mt-3">: Islam</div>
                </div>
                <div className="row kotakdetail2">
                  <div className="col mt-3 font-weight-bold">Status </div>
                  <div className="col mt-3">: Lajang</div>
                </div>
                <div className="row kotakdetail2">
                  <div className="col mt-3 font-weight-bold">
                    Pendidikan Terakhir{" "}
                  </div>
                  <div className="col mt-3">: SMA</div>
                </div>{" "}
              </Fade>
            </div>
            <div className="col-5 mt-3">
              <Fade bottom>
                {" "}
                <h4>Tertarik dengan profile [nama maid]? </h4>
                <div className="mt-5">Baca lebih lanjut mengenai rulenya </div>
                <a href="https://wa.me/+6281261055298">Disini</a>
              </Fade>
            </div>
          </div>
        </section>
        <Footer />
      </>
    );
  }
}
