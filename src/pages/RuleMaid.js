import Header from "parts/Header";
import React, { Component } from "react";
import Footer from "parts/Footer";
import BiodataMaid from "parts/BiodataMaid";
import Fade from "react-reveal/Fade";
import BookingForm from "parts/BookingForm";

import PageDetailDescription from "parts/PageDetailDescription";

export default class RuleMaid extends Component {
  componentDidMount() {
    window.title = "Temukand | Maid";
    window.scrollTo(0, 0);
  }
  render() {
    const breadcrumb = [
      { pageTitle: "Maid", pageHref: "" },
      { pageTitle: "Maid List", pageHref: "" },
    ];

    const settings = {
      dots: true,
      autoplay: true,
      infinite: true,
      slidesToShow: 2,
      slidesToScroll: 1,
    };
    return (
      <>
        <Header {...this.props}></Header>
        <Fade bottom>
          <div className="biodata">
            <div className="row">
              <div className="col-auto title">Rule</div>
            </div>
            <div className="row">
              <div className="col ruletext">1. Ini rule pertama</div>
            </div>
            <div className="row">
              <div className="col ruletext">2. Ini rule kedua</div>
            </div>
          </div>
        </Fade>

        <Footer />
      </>
    );
  }
}
