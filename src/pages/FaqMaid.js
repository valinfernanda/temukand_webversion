import React, { Component } from "react";
import Header from "parts/Header";
import Footer from "parts/Footer";

import FaqsMaid from "parts/FaqsMaid";

export default class FaqMaid extends Component {
  componentDidMount() {
    window.title = "Temukand | Maid";
    window.scrollTo(0, 0);
  }
  render() {
    return (
      <>
        <Header {...this.props}></Header>
        <FaqsMaid />

        <Footer />
      </>
    );
  }
}
