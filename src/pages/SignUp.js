import React, { Component } from "react";
import Header from "parts/Header";
import Footer from "parts/Footer";

import UserSignUp from "parts/UserSignUp";

export default class SignUp extends Component {
  constructor(props) {
    super(props);

    this.state = {
      nama: "",
      email: "",
      nohp: "",
      password: "",
    };
  }
  componentDidMount() {
    window.title = "Temukand | Sign Up";
    window.scrollTo(0, 0);
  }
  render() {
    const { nama, email, nohp, password } = this.state;
    return (
      <>
        <Header {...this.props}></Header>

        <UserSignUp />

        <Footer />
      </>
    );
  }
}
