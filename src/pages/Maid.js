import React, { Component } from "react";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import Header from "parts/Header";

import maid from "json/daftarMaid.json";
import ListMaid from "parts/ListMaid";
import HeroMaid from "parts/HeroMaid";
import Footer from "parts/Footer";
import ApplyAsMaid from "parts/ApplyAsMaid";

export default class Maid extends Component {
  componentDidMount() {
    window.title = "Temukand | Maid";
    window.scrollTo(0, 0);
  }
  render() {
    const breadcrumb = [
      { pageTitle: "Maid", pageHref: "" },
      { pageTitle: "Maid List", pageHref: "" },
    ];

    const settings = {
      dots: true,
      autoplay: true,
      infinite: true,
      slidesToShow: 2,
      slidesToScroll: 1,
    };
    return (
      <>
        <Header {...this.props}></Header>
        {/* <PageDetailTitle breadcrumb={breadcrumb} data={Maid}></PageDetailTitle> */}

        <HeroMaid />
        <ListMaid data={maid.daftarMaid} />
        <ApplyAsMaid />

        <Footer />
      </>
    );
  }
}
