import React from "react";
import { createBrowserHistory } from "history";
import { Router, Route, Switch } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import LandingPage from "pages/LandingPage";
import ListMaid from "pages/Maid";
import FaqMaid from "pages/FaqMaid";

import DetailsPage from "pages/DetailsPage";
import Checkout from "pages/Checkout";
import Example from "pages/Example";
import NotFound from "pages/404";

import "assets/scss/style.scss";
import MaidForm from "pages/MaidForm";
import SignUp from "pages/SignUp";
import MaidDetails from "pages/MaidDetails";
import RuleMaid from "pages/RuleMaid";

const history = createBrowserHistory({
  basename: process.env.PUBLIC_URL,
});

function App() {
  return (
    <div className="App">
      <Router history={history} basename={process.env.PUBLIC_URL}>
        <Switch>
          <Route exact path="/listmaid" component={ListMaid} />
          <Route exact path="/maidform" component={MaidForm} />
          <Route exact path="/signup" component={SignUp} />
          <Route exact path="/maiddetail" component={MaidDetails} />
          <Route exact path="/rulemaid" component={RuleMaid} />
          <Route exact path="/faqmaid" component={FaqMaid} />
          <Route exact path="/" component={LandingPage} />
          <Route exact path="/properties/:id" component={DetailsPage} />
          <Route path="/checkout" component={Checkout} />
          <Route path="/example" component={Example} />
          <Route path="*" component={NotFound} />
        </Switch>
      </Router>

      <ToastContainer></ToastContainer>
    </div>
  );
}

export default App;
